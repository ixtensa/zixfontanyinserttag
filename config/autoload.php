<?php

/**
 * Anpassung von contao-font-awesome-inserttag für eigene Fonts
 *
 * @package    contao-font-awesome-inserttag
 * @author     David Molineus <david.molineus@netzmacht.de>
 * @copyright  2015 netzmacht creative David Molineus
 * @license    LGPL 3.0
 * @filesource
 *
 */

/**
 * Register the namespaces
 */
ClassLoader::addNamespaces(array
(
	'IXTENSA',
));

ClassLoader::addClass(
    'IXTENSA\zixFontAnyInserttagHooks', 'system/modules/zixFontAnyInserttag/classes/zixFontAnyInserttagHooks.php'
);
