<?php

/**
 * Anpassung von contao-font-awesome-inserttag für eigene Fonts
 *
 * @package    contao-font-awesome-inserttag
 * @author     David Molineus <david.molineus@netzmacht.de>
 * @copyright  2015 netzmacht creative David Molineus
 * @license    LGPL 3.0
 * @filesource
 *
 */

/*
 * Register the hook.
 */
$GLOBALS['TL_HOOKS']['replaceInsertTags'][] = array('IXTENSA\zixFontAnyInserttagHooks', 'ixReplaceInsertTags');

/*
 * Template which is used for the insert tag replacing.
 */
$GLOBALS['TL_CONFIG']['faInsertTagTemplate'] = '<i class="fa fa-%s"></i>';
$GLOBALS['TL_CONFIG']['ixInsertTagTemplate'] = '<i class="ix ix-%s"></i>';
