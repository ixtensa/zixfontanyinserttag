Extension zixFontAnyInserttag
Package IXTENSA\FontAnyInsertTag

zixFontAnyInserttag ist an extension of font-awesome-inserttag.
It provides a Contao insert-tag for font icons.

Examples:
{{ix::phone}}
{{ix::phone foo}}
- 2nd param is added as class without prefix

config.php
Defines how the class will be embeded, eg:
<i class="ix ix-phone"></i>