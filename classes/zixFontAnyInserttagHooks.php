<?php

/**
 * Anpassung von contao-font-awesome-inserttag für eigene Fonts
 *
 * @package    contao-font-awesome-inserttag
 * @author     David Molineus <david.molineus@netzmacht.de>
 * @copyright  2015 netzmacht creative David Molineus
 * @license    LGPL 3.0
 * @filesource
 *
 */

namespace IXTENSA;

/**
 * Hooks contains the insert tag hook.
 *
 * @package IXTENSA\FontAnyInsertTag
 */
class zixFontAnyInserttagHooks
{
    /**
     * Replace the insert tag.
     *
     * Supported are followin options:
     * {{ix::phone}}
     * {{ix::phone 4x muted}}                       every entry sperated by space get an fa- prefix
     * {{ix::phone rotate-90 large::pull-left}}     2nd param is added as class without prefix
     *
     * @param string $tag The insert tag.
     *
     * @return bool|string
     */
    public function ixReplaceInsertTags($tag)
    {
    	$prefixAll = array("ix", "fa");
    	
    	foreach($prefixAll as $prefix) {
			if (strpos($tag, $prefix . '::') !== 0) {
				continue;
			}

			$parts = explode('::', $tag);
			$class = str_replace(' ', ' '.$prefix.'-', $parts[1]);

			if(isset($parts[2])) {
				$class .= ' ' . $parts[2];
			}

			if (!$class) {
				return '';
			}

			return sprintf($GLOBALS['TL_CONFIG'][$prefix . 'InsertTagTemplate'], $class);    	
    	}
    	
    	return false;
    }
}
